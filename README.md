PHP Yii2 Chatter
================
Real time chat module for Yii2

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist tmukherjee13/yii2-chatter "*"
```

or add

```
"tmukherjee13/yii2-chatter": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \tmukherjee13\chatter\AutoloadExample::widget(); ?>```